from dbutil import DBUtil
from aws_s3 import AWSS3
from api_handler import APIHandler
from utils import Util


def lambda_handler(event, context):
    
    bucket_name = "alemhealth-windows-upload-test-bucket"
    
    db = DBUtil()
    s3 = AWSS3()
    util = Util()
    
    # First read study items. If possible unprocessed only.
    studies = db.read_studies(bucket_name=bucket_name)
    
    # print(studies)
    
    study_guids = {}
    
    for study_object in studies:
        
        processed = study_object.get("processed", "0")
            
        facility_guid = study_object.get("facility_guid")
        study_name = study_object.get("study_name")
            
        bucket = study_object.get("bucket_name", "null")
        
        study_id = study_object.get("study_id", "null")
        
        # print(study_id)
            
        if study_id != "null":
            if bucket not in study_guids:
                study_guids[bucket] = {}
            if facility_guid not in study_guids[bucket]:
                study_guids[bucket][facility_guid] = {}
            study_guids[bucket][facility_guid][study_name] = study_id
            continue
        
        request_data = {
            "facility_guid": "bd98cb78-149f-43d3-9602-7fcb693363ea"
        }
        study_guid = APIHandler.create_study(request_data)
        if study_guid:
            updated = db.update_study(bucket, study_guid, study_name, facility_guid, processed)
            if updated:
                if bucket not in study_guids:
                    study_guids[bucket] = {}
                if facility_guid not in study_guids[bucket]:
                    study_guids[bucket][facility_guid] = {}
                study_guids[bucket][facility_guid][study_name] = study_guid
                print("Updated!")
            else:
                print("Not updated!")
                
    def get_study_guid(study_guids, bucket, facility, study_name):
        try:
            return study_guids[bucket][facility][study_name]
        except Exception as exp:
            return None
            
    # print(study_guids)
    
    study_items = db.read_study_items()
    
    # print(study_items)
    
    buckets = {}
    
    for study_item in study_items:
        # get_si = lambda si: si["N"] if "N" in si else si["S"]
        # pstatus = get_si(study_item["upload_status"])
        # if pstatus == "0":
        bucket_name = study_item.get("bucket_name")
        facility_guid = study_item.get("facility_guid")
        study_name = study_item.get("study_name")
        resource_name = study_item.get("resource_name")
        study_guid = study_item.get("study_id")
        
        if bucket_name not in buckets:
            buckets[bucket_name] = {}
            
        if facility_guid not in buckets[bucket_name]:
            buckets[bucket_name][facility_guid] = {}
            
        if study_name not in buckets[bucket_name][facility_guid]:
            buckets[bucket_name][facility_guid][study_name] = []
            
            
        buckets[bucket_name][facility_guid][study_name] += [{"resource": resource_name, "study_id": study_guid}]
            
            
    for bucket, bucket_item in buckets.items():
        for facility, facility_item in bucket_item.items():
            for study_name, study_resources in facility_item.items():
                for study_resource_object in study_resources:
                    study_resource = study_resource_object["resource"]
                    study_guid = study_resource_object["study_id"]
                    
                    study_guid = get_study_guid(study_guids, bucket, facility, study_name)
                    if not study_guid:
                        continue
                    
                    s3_content = s3.fetch_s3_object(bucket, study_resource)
                    
                    if s3_content:
                        uploaded = APIHandler.upload_file(study_guid, s3_content)
                        if uploaded:
                            updated = db.update_study_item(bucket, study_name, study_resource, upload_status=1, study_id=study_guid)
                            if updated:
                                data = {
                                    "active_status": 1
                                }
                                uploaded = APIHandler.update_study(study_guid, data)
                                if uploaded:
                                    print("Updated!")
                                else:
                                    print("Study update failed in API")
                            else:
                                print("Not Updated.")
