import boto3
from boto3.dynamodb.conditions import Key, Attr


class DBUtil(object):
    
    def __init__(self):
        self.region_name = "ap-southeast-1"
        self.endpoint_url = "http://localhost:8000"
        self.db = boto3.client("dynamodb", region_name=self.region_name)
    
    def create_resource_table(self):
        table = self.db.create_table(
        TableName="AWSResource",
        KeySchema=[
            {
                "AttributeName": "study_name",
                "KeyType": "HASH"  #Partition key
            },
            {
                "AttributeName": "resource_name",
                "KeyType": "RANGE"  #Sort key
            }
        ],
        AttributeDefinitions=[
            # {
            #     "AttributeName": "bucket_name",
            #     "AttributeType": "S"
            # },
            {
                "AttributeName": "study_name",
                "AttributeType": "S"
            },
            {
                "AttributeName": "resource_name",
                "AttributeType": "S"
            },
            # {
            #     "AttributeName": "upload_status",
            #     "AttributeType": "N"
            # },
            # {
            #     "AttributeName": "study_id",
            #     "AttributeType": "S"
            # },
    
        ],
        ProvisionedThroughput={
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10
        })
        
    def create_study_table(self):
        table = self.db.create_table(
        TableName="Study",
        KeySchema=[
            {
                "AttributeName": "study_name",
                "KeyType": "HASH"  #Partition key
            }
        ],
        AttributeDefinitions=[
            # {
            #     "AttributeName": "bucket_name",
            #     "AttributeType": "S"
            # },
            {
                "AttributeName": "study_name",
                "AttributeType": "S"
            },
            # {
            #     "AttributeName": "processed",
            #     "AttributeType": "N"
            # },
    
        ],
        ProvisionedThroughput={
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10
        })
        
    def create_all_table(self):
        self.create_resource_table()
        self.create_study_table()
      
      
    def insert_study_if_not_exists(self, study_name):
        try:
            study_table = self.db.put_item(
                TableName='Study',
                    Item={
                        'study_name':{
                            "S": study_name
                        },
                        'item_processed':{
                            "N": "0"
                        },
                    },
                    ConditionExpression='attribute_not_exists(study_name)'
                )
            return True
        except Exception as exp:
            return False
            
    def update_study(self, bucket, study_id, study_name, facility_id, processed):
        try:
            dynamodb = boto3.resource('dynamodb', region_name=self.region_name)

            table = dynamodb.Table('Study')
            
            response = table.update_item(
                Key={
                    'study_name': study_name
                },
                UpdateExpression="set study_id = :si, bucket_name=:bn, facility_guid=:fg, item_processed=:p",
                ExpressionAttributeValues={
                    ':si': study_id,
                    ':bn': bucket,
                    ':fg': facility_id,
                    ':p' : processed
                },
                ReturnValues="UPDATED_NEW"
            )
            return True
        except Exception as exp:
            print(str(exp))
            return False
        
    def insert_study_item(self, bucket_name, study_name, item_name, upload_status=0, study_id=None):
        try:
            item = {
                'bucket_name': {
                    "S": bucket_name
                },
                'study_name': {
                    "S": study_name
                },
                'resource_name': {
                    "S": item_name
                },
                'upload_status': {
                    "N": "%s" % upload_status
                }
            }
            if study_id:
                item["study_id"] = {
                    "S": study_id
                }
            response = self.db.put_item(
                    TableName="AWSResource",
                    Item=item
            )
            print("Item added.")
        except Exception as exp:
            print("Item add Failed.")
            
    def update_study_item(self, bucket_name, study_name, item_name, upload_status=0, study_id=None):
        try:
            if not study_id:
                study_id = 'null'
            
            dynamodb = boto3.resource('dynamodb', region_name=self.region_name)

            table = dynamodb.Table('AWSResource')
            
            response = table.update_item(
                Key={
                    'study_name': study_name,
                    'resource_name': item_name
                },
                UpdateExpression="set study_id = :si, bucket_name=:bn, upload_status=:us",
                ExpressionAttributeValues={
                    ':si': study_id,
                    ':bn': bucket_name,
                    ':us': upload_status
                },
                ReturnValues="UPDATED_NEW"
            )
            return True
        except Exception as exp:
            print(str(exp))
            
    def read_studies(self, bucket_name, upload_status=0):
        try:
            items = []
            dynamodb = boto3.resource('dynamodb', region_name=self.region_name)
            table = dynamodb.Table('Study')
            response = table.scan(
                    ConsistentRead=True
                )
            
            for item in response['Items']:
                items += [item]
            
            while True:
                if response.get('LastEvaluatedKey'):
                    response = table.scan(
                        ConsistentRead=True,
                        ExclusiveStartKey=response['LastEvaluatedKey']
                        )
                    items += response['Items']
                else:
                    break
            return items
            
        except Exception as exp:
            print(str(exp))
            pass
        
    def read_study_items(self, study_name=None, upload_status=None):
        try:
            # print(type(study_name))
            items = []
            dynamodb = boto3.resource('dynamodb', region_name=self.region_name)

            table = dynamodb.Table('AWSResource')
            
            response = table.scan(
                    FilterExpression=Key('upload_status').eq("0"),
                    ConsistentRead=True
                )
            
            for item in response['Items']:
                items += [item]
                
            while True:
                if response.get('LastEvaluatedKey'):
                    response = table.scan(
                        FilterExpression=Key('upload_status').eq("0"),
                        ConsistentRead=True,
                        ExclusiveStartKey=response['LastEvaluatedKey']
                        )
                    items += response['Items']
                else:
                    break
            
            return items
        except Exception as exp:
            print(str(exp))
            pass
        
        
    def read_study_items_v2(self, study_name, upload_status=0):
        try:
            response = self.db.batch_get_item(
                RequestItems={
                    'string': {
                        'Keys': [
                            {
                                'string': {
                                    'S': '%s' % study_name
                                }
                            },
                        ],
                        # 'AttributesToGet': [
                        #     'string',
                        # ],
                        'ConsistentRead': True,
                        # 'ProjectionExpression': 'string',
                        # 'ExpressionAttributeNames': {
                            # 'string': 'string'
                        # }
                    }
                },
                # ReturnConsumedCapacity='INDEXES'|'TOTAL'|'NONE'
            )
            # print(response)
        except Exception as exp:
            print(str(exp))
            pass
        